const form = document.getElementById('form')
const day = document.getElementById('day')
const month = document.getElementById('month')
const year = document.getElementById('year')
const resultYears = document.getElementById('result-years')
const resultMonths = document.getElementById('result-months')
const resultDays = document.getElementById('result-days')

let isValidationError = false

function validateField(field, callback) {
  const errorElement = field.closest('.input-row')

  if (!callback(parseInt(field.value).toString())) {
    errorElement.classList.add('error')
    isValidationError = true
  } else {
    errorElement.classList.remove('error')
  }
}

function calculateAge(birthDate) {
  const today = new Date()
  const birth = new Date(birthDate)

  let ageYears = today.getFullYear() - birth.getFullYear()
  let ageMonths = today.getMonth() - birth.getMonth()
  let ageDays = today.getDate() - birth.getDate()

  if (ageDays < 0) {
    ageMonths--
    const daysInBirthMonth = new Date(birth.getFullYear(), birth.getMonth() + 1, 0).getDate()
    ageDays += daysInBirthMonth
  }

  if (ageMonths < 0) {
    ageYears--
    ageMonths += 12
  }

  if (today < new Date(today.getFullYear(), birth.getMonth(), birth.getDate())) {
    ageDays--
  }

  return { years: ageYears, months: ageMonths, days: ageDays }
}

function handleSubmit(event) {
  event.preventDefault()

  const birthDate = new Date(year.value, month.value - 1, day.value)
  const currentDate = new Date()
  isValidationError = false

  validateField(day, value => value.match(/^(3[01]|[12][0-9]|[1-9])$/))
  validateField(month, value => value.match(/^(1[0-2]|[1-9])$/))
  validateField(year, () => year.value >= 1900 && birthDate < currentDate)

  if (isValidationError) return

  const { years, months, days } = calculateAge(birthDate)
  resultYears.textContent = years
  resultMonths.textContent = months
  resultDays.textContent = days
}

form.addEventListener('submit', handleSubmit)
